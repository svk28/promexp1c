#!/usr/bin/python3.5
import argparse
import configparser
import subprocess
from prometheus_client import start_http_server, Summary
from prometheus_client.core import GaugeMetricFamily, CounterMetricFamily, REGISTRY
from prometheus_client import CollectorRegistry, Gauge, push_to_gateway

parser = argparse.ArgumentParser(description='Prometheus exporter for 1C:Platform')
parser.add_argument('-c', '--config', help='config file full path')
args = parser.parse_args()

def ReadConfig(conf_file):
    config = configparser.ConfigParser()
    config.read(conf_file)
    return config

def GetCluster(rac_cmd, server):
    cmd = rac_cmd + ' cluster list ' + server
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    cluster_list = {}
    for line in proc.stdout.readlines():
        out = line.decode('utf-8').strip().split(':')
        if out[0].strip() == 'cluster':
            cluster_id = out[1].strip()
        if out[0].strip() == 'name':
            cluster_list[cluster_id] = out[1].strip()
    #proc.terminate()
    return(cluster_list)

def GetInfoBase(cluster, rac_cmd, server):
    cmd = rac_cmd + ' infobase summary --cluster=' + cluster + ' list ' + server
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    infobase_list[cluster] = {}
    for line in proc.stdout.readlines():
        out = line.decode('utf-8').strip().split(':')
        if out[0].strip() == 'infobase':
            infobase_id = out[1].strip()
        if out[0].strip() == 'name':
            infobase_list[cluster][infobase_id] = out[1].strip()
    #proc.terminate()

def GetClusterSessions(cluster, rac_cmd, server):
    cmd = '{} session list --cluster={} {}'.format(rac_cmd, cluster, server)
    proc = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    for line in proc.stdout.readlines():
        out = line.decode('utf-8').strip().split(':')
        if out[0].strip() == 'session':
            session_id = out[1].strip()
        if out[0].strip() == 'infobase':
            infobase_id = out[1].strip()
            if sessions_list.get(infobase_id):
                sessions_list[infobase_id].append(session_id)
            else:
                sessions_list[infobase_id] = [session_id]
    return(sessions_list)

def GetInfobaseSessions(cluster, rac_cmd, server, infobase):
    cmd = rac_cmd + ' infobase summary --cluster=' + cluster + ' list ' + server
    return
def GetInfobaseConnections():
    return

def InfobaseSessionsCount(lst):
    return(len(lst))

if not args.config:
    parser.print_help()
    exit()
else:
    servers_list = ReadConfig(args.config)
    cluster_list = {}
    infobase_list = {}
    global sessions_list
    sessions_list = {}
    registry = CollectorRegistry()
    for server in servers_list:
        #print('server:' + server)
        server_address = servers_list[server]['address'] + ':' + servers_list[server]['port']
        rac_cmd = servers_list[server]['rac_cmd']
        cluster_list[server] = GetCluster(rac_cmd, server_address)
    for key in cluster_list:
        for cluster in cluster_list[key]:
            #print('cluster - ' + cluster)
            GetInfoBase(cluster, rac_cmd, server_address)
            #print(infobase_list[cluster])
            GetClusterSessions(cluster, rac_cmd, server_address)
            for infobase in sessions_list:
                metric_name = 'infobase_' + infobase_list[cluster][infobase] + '_sessions_counter'
                #metric_name = 'infobase_' + infobase.replace('-', '_') + '_sessions_counter_total'
                print(metric_name)
                g = Gauge(metric_name, 'Infobases session counter', registry=registry)
                g.set(len(sessions_list[infobase]))
                push_to_gateway('192.168.5.105:9091', job='test_1c', registry=registry)
